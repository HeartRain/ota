# ./gendiff.sh &>output.log
path_benchmark="benchmark/"
path_bin="OTA/"
flod_name="aos"

aos_root="/home/tong/AliOS-Things-rel_2.1.0_bsidff"
case_root="/mnt/hgfs/vmShareFlod/benchmark/case_all"
des_root="/mnt/hgfs/vmShareFlod/benchmark"
catalog="/memory/bsdiff"
diff_cli="genbsdiffDiff.sh"


for i in {1..8}
do  
    rm -r ${des_root}/case${i}/${catalog}/oldapp
    rm ${des_root}/case${i}/${catalog}/old*
    rm ${des_root}/case${i}/${catalog}/new2old.bin
    mkdir ${des_root}/case${i}/${catalog}/oldapp
    
    if [ $i -lt 7 ];then
        rm -r ${des_root}/case${i}/${catalog}/newapp
        mkdir ${des_root}/case${i}/${catalog}/newapp
    fi
    
    cd ${case_root}/case${i}/oldapp
    for f in $(ls)
    do
        rm -r ${aos_root}/app/example/${f}
        cp -r ${case_root}/case${i}/oldapp/${f} ${aos_root}/app/example/${f}
        echo cd ${aos_root}
        cd ${aos_root}
        echo aos make ${f}@developerkit -c config
        aos make ${f}@developerkit -c config
        aos make clean
        aos make 
        cp -r ${aos_root}/out/${f}@developerkit/binary ${des_root}/case${i}/${catalog}/oldapp
        cd ${des_root}/case${i}/${catalog}
        cp oldapp/binary/${f}@developerkit.ota.bin old.ota.bin 
        cp oldapp/binary/${f}@developerkit.bin old.bin 
    done
    if [ $i -lt 7 ];then
        cd ${case_root}/case${i}/newapp
        for f in $(ls)
        do
            rm -r ${aos_root}/app/example/${f}
            cp -r ${case_root}/case${i}/newapp/${f} ${aos_root}/app/example/${f}
            cd ${aos_root}
            aos make ${f}@developerkit -c config
            aos make clean
            aos make 
            cp -r ${aos_root}/out/${f}@developerkit/binary ${des_root}/case${i}/${catalog}/newapp
            cd ${des_root}/case${i}/${catalog}
            cp newapp/binary/${f}@developerkit.ota.bin new.ota.bin 
            cp newapp/binary/${f}@developerkit.bin new.bin 
        done
    fi
    cp ${des_root}/case${i}/${catalog}/old.ota.bin /home/tong/temp/
    cp ${des_root}/case${i}/${catalog}/new.ota.bin /home/tong/temp/
    cd /home/tong/temp/
    ./${diff_cli}
    cp old2new.bin ${des_root}/case${i}/${catalog}/
    cp new2old.bin ${des_root}/case${i}/${catalog}/
done
