
#include <stdio.h>
#include "head.h"
#include <stdlib.h>
#include<limits.h>

Stack* initStack()
{
	Stack *s = (Stack *)malloc(sizeof(Stack));
	s->pre = NULL;
	s->preGray = NULL;
	return s;
}
 
void push(Stack **s, int value)
{
	 // printf("push%d\n",value);
	Stack *n = (Stack *)malloc(sizeof(Stack));
	n->pre = *s;
	n->value = value;
	*s = n;
}
 
int pop(Stack **s)
{
	if ((*s)->pre == NULL)
	{
		// printf("pop error\n");
		return INT_MAX;
	}
 
	int value = (*s)->value;
	Stack *pre = (*s)->pre;
	free(*s);
	*s = pre;
	 // printf("pop%d\n",value);
	return value;
}

int peek(Stack *s){
	if (s->pre == NULL)
	{
		// printf("peek error\n");
		return INT_MAX;
	}
	return s->value;
}
 
int isEmpty(Stack *s)
{
	if (s->pre == NULL)
	{
		return 1;
	}
	return 0;
}
 
void destroyStack(Stack **s)
{
	while (*s != NULL)
	{
		Stack *pre = (*s)->pre;
		free(*s);
		*s = pre;
	}
}

void  printStack(Stack *s){
	while (s != NULL)
	{
		// printf("%d/",s->value);
		s = s->pre;
	}
	// printf("\n");
}