### 适配设备端flash擦写特性（最小擦除单位和写入对齐）

### 以最小擦除单位为block进行拓扑排序（可以自定义块的大小）

### 需要用到lzma的库文件，ubuntu下可以用以下命令安装

        apt install liblzma-dev

### 生成可执行文件

```
	make	
```

### 生成差分包

        ./s2diff oldfilename newfilename patchfilename

### 设备端更新
        ./s2patch oldfilename patchfilename

