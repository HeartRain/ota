#include <stdio.h>
#include <stdlib.h>
#include "head.h"
Block newBlock(int start,int end,int maxCmdCount){
    Block block;
    block.cmdCount = 0;
    block.startPos = start;
    block.endPos = end;
    block.maxCmdCount = maxCmdCount;
    block.cmds = malloc(sizeof(Cmd)*maxCmdCount);
    return block;
}
Cmd newAdd(off_t t,off_t l){
    Cmd cmd;
    cmd.type = 0;
    cmd.t = t;
    cmd.f = -1;
    cmd.l = l;
    return cmd;
}
Cmd newCopy(off_t f,off_t t,off_t l){
    Cmd cmd;
    cmd.type = 1;
    cmd.t = t;
    cmd.f = f;
    cmd.l = l;
    return cmd;
}
int addCmdToBlock(Block *block,Cmd cmd){
    if(block->cmdCount>=block->maxCmdCount){
        printf("cmdCount > maxCmdCount");
        return -1;
    }
    block->cmds[block->cmdCount] = cmd;
    block->cmdCount = block->cmdCount+1;
    return 1;
}