# ./gendiff.sh &>output.log
diff_cli="genbsdiffDiff.sh"
aos_root="/home/tong/AliOS-Things-rel_2.1.0_bsidff"
case_root="/mnt/hgfs/vmShareFlod/benchmark"
catalog="/time/bsdiff"

for i in {1..1}
do
    cd ${case_root}/case${i}/${catalog}
    cp newapp/binary/otaapp@developerkit.ota.bin new.ota.bin
    cp newapp/binary/otaapp@developerkit.bin new.bin
    cp oldapp/binary/otaapp@developerkit.ota.bin old.ota.bin
    cp oldapp/binary/otaapp@developerkit.bin old.bin

    cp newapp/binary/linkkitapp@developerkit.ota.bin new.ota.bin
    cp newapp/binary/linkkitapp@developerkit.bin new.bin
    cp oldapp/binary/linkkitapp@developerkit.ota.bin old.ota.bin
    cp oldapp/binary/linkkitapp@developerkit.bin old.bin
    cp old.ota.bin /home/tong/temp
    cp new.ota.bin /home/tong/temp
    cd /home/tong/temp
    ./${diff_cli}
    cp old2new.bin ${case_root}/case${i}/${catalog}
    cp new2old.bin ${case_root}/case${i}/${catalog}
done
exit

