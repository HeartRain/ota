#if 0
__FBSDID("$FreeBSD: src/usr.bin/bsdiff/bsdiff/bsdiff.c,v 1.1 2005/08/06 01:59:05 cperciva Exp $");
#endif

#include <sys/types.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <lzma.h>
#include <stdbool.h>
#include <errno.h>
#include "head.h"

#define MIN(x,y) (((x)<(y)) ? (x) : (y))

#define blockSize (4*1024) //最小擦除单位

#define HEADERFILENAME "tempheaderfile"
#define CTRLFILENAME "tempctrlfile"
#define DATAFILENAME "tempdatafile"

#define CTRLFILENAMECOMPRESS "tempctrlfile.xz"
#define DATAFILENAMECOMPRESS "tempdatafile.xz"

off_t getFileSize(char *file){
	FILE *f;
	f = fopen(file,"rb");
	fseek(f,0,SEEK_END);

	off_t fileSize = ftell(f);
	// printf("size of %s:%ld",file,fileSize);
	fclose(f);
	return fileSize;

}

int mergeFile(char *in1,char *in2,char *out){
	FILE *f_in,*f_out;
	int n;
	char buf[4096];

	//open out file
	f_out = fopen(out,"wb");
	if(!f_out) return 0;

	//open first in file
	f_in = fopen(in1,"rb");
	if(!f_in) return 0;
	while(n=fread(buf,1,sizeof(buf),f_in))
	{
		fwrite(buf,1,n,f_out);
	}
	fclose(f_in);

	//open second in file
	f_in = fopen(in2,"rb");
	if(!f_in) return 0;
		while(n=fread(buf,1,sizeof(buf),f_in))
	{
		fwrite(buf,1,n,f_out);
	}
	fclose(f_in);
	fclose(f_out);
	return 1;
}

static void split(off_t *I,off_t *V,off_t start,off_t len,off_t h)
{
	off_t i,j,k,x,tmp,jj,kk;

	if(len<16) {
		for(k=start;k<start+len;k+=j) {
			j=1;x=V[I[k]+h];
			for(i=1;k+i<start+len;i++) {
				if(V[I[k+i]+h]<x) {
					x=V[I[k+i]+h];
					j=0;
				};
				if(V[I[k+i]+h]==x) {
					tmp=I[k+j];I[k+j]=I[k+i];I[k+i]=tmp;
					j++;
				};
			};
			for(i=0;i<j;i++) V[I[k+i]]=k+j-1;
			if(j==1) I[k]=-1;
		};
		return;
	};

	x=V[I[start+len/2]+h];
	jj=0;kk=0;
	for(i=start;i<start+len;i++) {
		if(V[I[i]+h]<x) jj++;
		if(V[I[i]+h]==x) kk++;
	};
	jj+=start;kk+=jj;

	i=start;j=0;k=0;
	while(i<jj) {
		if(V[I[i]+h]<x) {
			i++;
		} else if(V[I[i]+h]==x) {
			tmp=I[i];I[i]=I[jj+j];I[jj+j]=tmp;
			j++;
		} else {
			tmp=I[i];I[i]=I[kk+k];I[kk+k]=tmp;
			k++;
		};
	};

	while(jj+j<kk) {
		if(V[I[jj+j]+h]==x) {
			j++;
		} else {
			tmp=I[jj+j];I[jj+j]=I[kk+k];I[kk+k]=tmp;
			k++;
		};
	};

	if(jj>start) split(I,V,start,jj-start,h);

	for(i=0;i<kk-jj;i++) V[I[jj+i]]=kk-1;
	if(jj==kk-1) I[jj]=-1;

	if(start+len>kk) split(I,V,kk,start+len-kk,h);
}

static void qsufsort(off_t *I,off_t *V,u_char *old,off_t oldsize)
{
	off_t buckets[256];
	off_t i,h,len;

	for(i=0;i<256;i++) buckets[i]=0;
	for(i=0;i<oldsize;i++) buckets[old[i]]++;
	for(i=1;i<256;i++) buckets[i]+=buckets[i-1];
	for(i=255;i>0;i--) buckets[i]=buckets[i-1];
	buckets[0]=0;

	for(i=0;i<oldsize;i++) I[++buckets[old[i]]]=i;
	I[0]=oldsize;
	for(i=0;i<oldsize;i++) V[i]=buckets[old[i]];
	V[oldsize]=0;
	for(i=1;i<256;i++) if(buckets[i]==buckets[i-1]+1) I[buckets[i]]=-1;
	I[0]=-1;

	for(h=1;I[0]!=-(oldsize+1);h+=h) {
		len=0;
		for(i=0;i<oldsize+1;) {
			if(I[i]<0) {
				len-=I[i];
				i-=I[i];
			} else {
				if(len) I[i-len]=-len;
				len=V[I[i]]+1-i;
				split(I,V,i,len,h);
				i+=len;
				len=0;
			};
		};
		if(len) I[i-len]=-len;
	};

	for(i=0;i<oldsize+1;i++) I[V[i]]=i;
}

static off_t matchlen(u_char *old,off_t oldsize,u_char *new,off_t newsize)
{
	off_t i;

	for(i=0;(i<oldsize)&&(i<newsize);i++)
		if(old[i]!=new[i]) break;

	return i;
}

static off_t search(off_t *I,u_char *old,off_t oldsize,
		u_char *new,off_t newsize,off_t st,off_t en,off_t *pos)
{
	off_t x,y;

	if(en-st<2) {
		x=matchlen(old+I[st],oldsize-I[st],new,newsize);
		y=matchlen(old+I[en],oldsize-I[en],new,newsize);

		if(x>y) {
			*pos=I[st];
			return x;
		} else {
			*pos=I[en];
			return y;
		}
	};

	x=st+(en-st)/2;
	if(memcmp(old+I[x],new,MIN(oldsize-I[x],newsize))<0) {
		return search(I,old,oldsize,new,newsize,x,en,pos);
	} else {
		return search(I,old,oldsize,new,newsize,st,x,pos);
	};
}

static void offtout(off_t x,u_char *buf)
{
	off_t y;

	if(x<0) y=-x; else y=x;

		buf[0]=y%256;y-=buf[0];
	y=y/256;buf[1]=y%256;y-=buf[1];
	y=y/256;buf[2]=y%256;y-=buf[2];
	y=y/256;buf[3]=y%256;y-=buf[3];
	y=y/256;buf[4]=y%256;y-=buf[4];
	y=y/256;buf[5]=y%256;y-=buf[5];
	y=y/256;buf[6]=y%256;y-=buf[6];
	y=y/256;buf[7]=y%256;

	if(x<0) buf[7]|=0x80;
}

int main(int argc,char *argv[])
{
	//bsdiff原有变量
	int fd;
	u_char *old,*new;
	off_t oldsize,newsize;
	off_t *I,*V;
	off_t scan,pos,len;
	off_t lastscan,lastpos,lastoffset;
	off_t oldscore,scsc;
	off_t s,Sf,lenf,Sb,lenb;
	off_t overlap,Ss,lens;
	off_t dblen,eblen;
	u_char *data;
	u_char buf[8];
	u_char header[40];
	FILE * pf,*tempf;

	//inPlace后新增变量
	off_t datapos=0;
	int *order;//用来保存还原时block的顺序
	char *tempfilename;
	char cmdString[200];

	Block *blocks;
	int blockCount=0;

	if(argc!=4) errx(1,"usage: %s oldfile newfile patchfile\n",argv[0]);


	/* Allocate oldsize+1 bytes instead of oldsize bytes to ensure
		that we never try to malloc(0) and get a NULL pointer */
	if(((fd=open(argv[1],O_RDONLY,0))<0) ||
		((oldsize=lseek(fd,0,SEEK_END))==-1) ||
		((old=malloc(oldsize+1))==NULL) ||
		(lseek(fd,0,SEEK_SET)!=0) ||
		(read(fd,old,oldsize)!=oldsize) ||
		(close(fd)==-1)) err(1,"%s",argv[1]);



	if(((I=malloc((oldsize+1)*sizeof(off_t)))==NULL) ||
		((V=malloc((oldsize+1)*sizeof(off_t)))==NULL)) err(1,NULL);

	qsufsort(I,V,old,oldsize);

	free(V);

	/* Allocate newsize+1 bytes instead of newsize bytes to ensure
		that we never try to malloc(0) and get a NULL pointer */
	if(((fd=open(argv[2],O_RDONLY,0))<0) ||
		((newsize=lseek(fd,0,SEEK_END))==-1) ||
		((new=malloc(newsize+1))==NULL) ||
		(lseek(fd,0,SEEK_SET)!=0) ||
		(read(fd,new,newsize)!=newsize) ||
		(close(fd)==-1)) err(1,"%s",argv[2]);

	if(ifdebug) printf("old:%s\n",old);
	if(ifdebug) printf("new:%s\n",new);

	blockCount = newsize/blockSize;
	if(newsize%blockSize>0) blockCount++;
	blocks = malloc(sizeof(Block)*blockCount);
	for(int i=0;i<blockCount;i++){
		off_t start = i*blockSize;
		off_t end = MIN(newsize-1,start+blockSize-1);
		blocks[i] = newBlock(start,end,blockSize);
	}

	data = malloc(newsize*sizeof(u_char));

	
	scan=0;len=0;
	lastscan=0;lastpos=0;lastoffset=0;
	int blockindex=0;
	while(scan<newsize) {
		oldscore=0;

		for(scsc=scan+=len;scan<newsize;scan++) {
			len=search(I,old,oldsize,new+scan,newsize-scan,
					0,oldsize,&pos);

			for(;scsc<scan+len;scsc++)
			if((scsc+lastoffset<oldsize) &&
				(old[scsc+lastoffset] == new[scsc]))
				oldscore++;

			if(((len==oldscore) && (len!=0)) || 
				(len>oldscore+8)) break;

			if((scan+lastoffset<oldsize) &&
				(old[scan+lastoffset] == new[scan]))
				oldscore--;
		};

		if((len!=oldscore) || (scan==newsize)) {
			s=0;Sf=0;lenf=0;
			for(int i=0;(lastscan+i<scan)&&(lastpos+i<oldsize);) {
				if(old[lastpos+i]==new[lastscan+i]) s++;
				i++;
				if(s*2-i>Sf*2-lenf) { Sf=s; lenf=i; };
			};

			lenb=0;
			if(scan<newsize) {
				s=0;Sb=0;
				for(int i=1;(scan>=lastscan+i)&&(pos>=i);i++) {
					if(old[pos-i]==new[scan-i]) s++;
					if(s*2-i>Sb*2-lenb) { Sb=s; lenb=i; };
				};
			};

			if(lastscan+lenf>scan-lenb) {
				overlap=(lastscan+lenf)-(scan-lenb);
				s=0;Ss=0;lens=0;
				for(int i=0;i<overlap;i++) {
					if(new[lastscan+lenf-overlap+i]==
					   old[lastpos+lenf-overlap+i]) s++;
					if(new[scan-lenb+i]==
					   old[pos-lenb+i]) s--;
					if(s>Ss) { Ss=s; lens=i+1; };
				};

				lenf+=lens-overlap;
				lenb-=lens;
			};


			//保存copy指令
			Copy cp;cp.fd=dblen;cp.ff=lastpos;cp.t=lastscan;cp.l=lenf;
			if(lenf!=0){
				int f = lastpos;
				int l = lenf;
				int t = lastscan;
				while(l>0){
					if(t+l-1>=blocks[blockindex].endPos){
						Cmd cmd = newCopy(f,t,blocks[blockindex].endPos-t+1);
						addCmdToBlock(blocks+blockindex,cmd);
						f += cmd.l;
						t += cmd.l;
						l -= cmd.l;
						blockindex++;
					}else{
						Cmd cmd = newCopy(f,t,l);
						addCmdToBlock(blocks+blockindex,cmd);
						f += cmd.l;
						t += cmd.l;
						l -= cmd.l;
					}
				}
			}

			//保存add指令
			Add add;add.fe=eblen;add.t=lastscan+lenf;add.l=(scan-lenb)-(lastscan+lenf);
			if(add.l!=0){
				int l = (scan-lenb)-(lastscan+lenf);
				int t = lastscan+lenf;
				while(l>0){
					if(t+l-1>=blocks[blockindex].endPos){
						Cmd cmd = newAdd(t,blocks[blockindex].endPos-t+1);
						addCmdToBlock(blocks+blockindex,cmd);
						t += cmd.l;
						l -= cmd.l;
						blockindex++;
					}else{
						Cmd cmd = newAdd(t,l);
						addCmdToBlock(blocks+blockindex,cmd);
						t += cmd.l;
						l -= cmd.l;
					}
				}
			}

			dblen+=lenf;
			eblen+=(scan-lenb)-(lastscan+lenf);

			lastscan=scan-lenb;
			lastpos=pos-lenb;
			lastoffset=pos-scan;
		};
	};
	order = malloc(sizeof(int)*(blockCount+1));


	//对block块重新排序，消除block块之间的依赖关系
	reOrder(blocks,blockCount,order);

	

/* Create the patch file */


	if ((tempf = fopen(CTRLFILENAME, "w")) == NULL)
		err(1, "%s", argv[3]);

	/**
	 * ctrl(x,y,z)
	 * x:length of copy
	 * y:length of add
	 * z:legnth of pointer need to move
	*/

	off_t lastend=0;
	off_t dataLen = 0;
	for(int i=0;i<blockCount;i++){
		int n = order[i];
		offtout(n,buf);
		fwrite(buf, 8, 1, tempf);
		Cmd *cmds = blocks[n].cmds;
		int j=0;
		while(j<blocks[n].cmdCount){
			int x=0,y=0,z=0;
			if(cmds[j].type==1){
				Cmd c = cmds[j];
				x = c.l;
				z = c.f - lastend;
				for(int k=0;k<x;k++){
					data[dataLen+k] = new[c.t+k] - old[c.f+k];
				}
				lastend = c.f+c.l;
				dataLen += x;
				j++;
			}
			if(j<blocks[n].cmdCount&&cmds[j].type==0){
				Cmd c = cmds[j];
				y = c.l;
				for(int k=0;k<y;k++){
					data[dataLen+k] = new[c.t+k];
				}
				dataLen += y;
				j++;
			}


			offtout(x,buf);
			fwrite(buf, 8, 1, tempf);

			offtout(y,buf);
			fwrite(buf, 8, 1, tempf);

			offtout(z,buf);
			fwrite(buf, 8, 1, tempf);
			if(ifdebug) printf("[%d,%d,%d]\n",x,y,z);
		}

	}



	if (fclose(tempf))
		err(1, "fclose");

	sprintf(cmdString, "%s%s", "xz --check=crc32 --lzma2=preset=6e,dict=16KiB ", CTRLFILENAME);
	system(cmdString);


	if ((tempf = fopen(DATAFILENAME, "w")) == NULL)
		err(1, "%s", argv[3]);
	
	fwrite(data, dataLen, 1, tempf);

	if(ifdebug)	printf("dataLen:%ld\n",dataLen);
	if (fclose(tempf))
		err(1, "fclose");

	sprintf(cmdString, "%s%s", "xz --check=crc32 --lzma2=preset=6e,dict=16KiB ", DATAFILENAME);
	system(cmdString);

	off_t ctrlSize_compress = getFileSize(CTRLFILENAMECOMPRESS);
	off_t dataSize_compress = getFileSize(DATAFILENAMECOMPRESS);


	if ((tempf = fopen(HEADERFILENAME, "w")) == NULL)
		err(1, "%s", argv[3]);
	
	/**
	 * diffFile : header + ctrl + data
	 * header
	 * 0-7 ： "BSDIFF40"
	 * 8-15 : size of newfile
	 * 16-23 : size of block
	 * 24-31 : count of block
	 * 32-39 : pos of data in diff
	 */
	memcpy(header,"BSDIFF40",8);
	offtout(newsize, header + 8);
	offtout(blockSize, header + 16);
	offtout(blockCount, header + 24);
	offtout(40 + ctrlSize_compress, header + 32);

	if (fwrite(header, 40, 1, tempf) != 1)
		err(1, "fwrite(%s)", argv[3]);

	if (fclose(tempf))
		err(1, "fclose");



	tempfilename = malloc(strlen(argv[3]) + 20);
    sprintf(tempfilename, "%s%s", argv[3], "_tempfile");

	int ret;
	ret = mergeFile(HEADERFILENAME,CTRLFILENAMECOMPRESS,tempfilename);
	if(ret==0) printf("merge error\n");
	ret = mergeFile(tempfilename,DATAFILENAMECOMPRESS,argv[3]);
	if(ret==0) printf("merge error\n");

	remove(HEADERFILENAME);
	remove(tempfilename);
	remove(CTRLFILENAME);
	remove(CTRLFILENAMECOMPRESS);
	remove(DATAFILENAME);
	remove(DATAFILENAMECOMPRESS);


	/* Free the memory we used */
	free(data);
	free(I);
	free(old);
	free(new);


	return 0;
}
