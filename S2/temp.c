#if 0
__FBSDID("$FreeBSD: src/usr.bin/bsdiff/bsdiff/bsdiff.c,v 1.1 2005/08/06 01:59:05 cperciva Exp $");
#endif

#include <sys/types.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include "head.h"

int main(int argc,char *argv[])
{
	long *temp;
	int num = 1000;
	temp = malloc(num*sizeof(long));
	printf("malloc:%d\n",num*sizeof(long));
	printf("free:%d\n",_msize(temp));
	free(temp);

	return 0;
}
