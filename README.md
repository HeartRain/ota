#### S2算法可以用于物联网设备原地差分更新，具有占用内存、存储小的特点。

#### S2文件夹下是算法源码

#### nbpatch.c文件是将我们的算法集成进AOS2.1时需要替换的文件，目录是middleware/uagent/ota/2nd_boot/nbpatch/，修改了其中的nbpatch_section（），由于是自己做实验，没有进行CRC检验。

#### S2原理是一个简单的ppt用于介绍算法基本原理

#### evalution文件夹下是实验的一些数据

